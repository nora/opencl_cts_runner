#!/usr/bin/env python3
import argparse
import itertools
import json
import multiprocessing
import os
from pathlib import Path
import re
import subprocess
import sys
import tempfile
import time
from tqdm import tqdm

os.sync()
CTS_PATH=Path('/home/kherbst/git/OpenCL-CTS/build/test_conformance')
TIMEOUT=2400

passing = [
]

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--compilation-mode', type=str, choices=['online', 'binary', 'spir-v'], default='online')
parser.add_argument('-d', '--dry-run', action='store_true')
parser.add_argument('-f', '--test-file', action='append', type=str, default=[])
parser.add_argument('-i', '--include', action='append', type=str, default=[])
parser.add_argument('-j', '--jobs', type=int, default=multiprocessing.cpu_count())
parser.add_argument('-s', '--singlethreaded', action='store_true')
parser.add_argument('-v', '--version', type=str, default='1.2')
parser.add_argument('-w', '--wimpy', action='store_true')
parser.add_argument('-x', '--exclude', action='append', type=str, default=[])
args = parser.parse_args()

if len(args.version):
	os.environ['CLOVER_DEVICE_VERSION_OVERRIDE'] = args.version
	os.environ['CLOVER_DEVICE_CLC_VERSION_OVERRIDE'] = args.version
	os.environ['CLOVER_PLATFORM_VERSION_OVERRIDE'] = args.version

os.environ['CL_TEST_SINGLE_THREADED'] = '1'
if args.wimpy:
	os.environ['CL_WIMPY_MODE'] = '1'

if len(args.include) == 0:
	args.include = passing
elif args.include[0] == 'all':
	args.include = []

# add current dir to PATH for cl_offline_compiler
os.environ["PATH"] += ':' + os.path.dirname(__file__)
compilation_modes = []

class DumpAsJson:
	def __str__(self):
		return json.dumps(self, cls=TestEncoder, indent=4, sort_keys=True)

	def __repr__(self):
		return str(self)

class TestResults(DumpAsJson):
	def __init__(self):
		self.crashes = []
		self.fails = []
		self.passes = []
		self.timeouts = []

result = TestResults()

class TestResult(DumpAsJson):
	def __init__(self, returncode, stdout, stderr, id, name, timedout):
		self.id = id
		self.name = name
		self.returncode = returncode
		self.timedout = timedout
		self.stdout = stdout
		self.stderr = stderr

	def __lt__(self, other):
		return self.id + ' ' + self.name < other.id + ' ' + other.name

class TestSuite(DumpAsJson):
	__regex_def = re.compile('^\s*Test names.*:$\n', re.M)
	__regex_half = re.compile('^\s*-h\s*Help.*$\n', re.M)
	__regex_math = re.compile('^Math function names:$\n', re.M)
	__regex_printf = re.compile('\s*default is to run the full test on the default device')

	__procs = []

	def __init__(self, id, path, tests):
		self.id = id
		self.path = Path(path)
		assert(len(tests) > 0)
		self.tests = tests
		self.has_comp_mode = id in compilation_modes

	def execute(self, test):
		if args.dry_run:
			return TestResult(0, '', '', self.id, test, False)

		timedout = False
		add_args = []
		if self.id == 'spirv':
			add_args = ['--spirv-binaries-path', '%s/../../test_conformance/spirv_new/spirv_bin/' % CTS_PATH]

		if self.has_comp_mode:
			binary_dir = tempfile.TemporaryDirectory()
			add_args += ['--compilation-mode', args.compilation_mode, '--compilation-cache-path', binary_dir.name]

		try:
			proc = subprocess.Popen([self.path] + test.split() + add_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
			TestSuite.__procs.append(proc)
			stdout, stderr = proc.communicate(timeout=TIMEOUT)
		except subprocess.TimeoutExpired as err:
			stdout = [err.stdout if err.stdout is None else err.stdout.decode("utf-8")]
			stderr = [err.stderr if err.stderr is None else err.stderr.decode("utf-8")]
			proc.kill()
			timedout = True
		except:
			print('Unexpected error:', sys.exc_info()[0])

		TestSuite.__procs.remove(proc)
		if self.has_comp_mode:
			del binary_dir
		return TestResult(proc.returncode, stdout, stderr, self.id, test, timedout)

	@classmethod
	def create(cls, id, file):
		path = CTS_PATH.joinpath(file)
		assert path.exists(), "%s not found" % path

		# we have to create the list ourselves
		if id == 'conversions':
			params = [
				['char', 'uchar', 'short', 'ushort', 'int', 'uint', 'long', 'ulong', 'float', 'double'], #dest
				['', '_sat'], #sat
				['', '_rte', '_rtp', '_rtn', '_rtz'], #mode
				['char', 'uchar', 'short', 'ushort', 'int', 'uint', 'long', 'ulong', 'float', 'double'], #src
			]
			m = lambda t: '%s%s%s_%s' % t
			f = lambda t: not ((t[0] == 'float' or t[0] == 'double') and t[1] == '_sat')

			combinations = map(m, filter(f, itertools.product(*params)))
			return TestSuite(id, path, list(combinations))

		args = [path]
		if id == 'math_brute_force':
			args.append('-p')
			regex = cls.__regex_math
		elif id == 'printf':
			args.append('-h')
			regex = cls.__regex_printf
		elif id == 'half':
			args.append('-h')
			regex = cls.__regex_half
		else:
			args.append('-h')
			regex = cls.__regex_def

		res = subprocess.run(args, capture_output=True)
		proc_output = res.stdout.decode()
		match = re.search(regex, proc_output)
		assert(match)
		output = proc_output[match.end():]
		output = output.replace('You may also use appropriate CL_ channel type and ordering constants.', '')

		# images have some subtests as well after parsing
		if id.startswith('images_'):
			subtests = ['', 'max_images', 'randomize', 'small_images', 'use_pitches']
			subtests = filter(lambda t: t in proc_output, subtests)

			params = [
				output.split(),
				subtests,
			]

			if id == ('images_image_streams'):
				params.append(['CL_FILTER_LINEAR', 'CL_FILTER_NEAREST'])
				m = lambda t: '%s %s %s' % t
			else:
				m = lambda t: '%s %s' % t

			combinations = map(m, itertools.product(*params))

			return TestSuite(id, path, list(combinations))

		return TestSuite(id, path, output.split())

	@classmethod
	def killAll(cls):
		for p in cls.__procs:
			if p.poll() is not None:
				return
			p.terminate()
			p.wait(.01)
			if p.poll() is not None:
				return
			p.kill()

class TestEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, TestResults):
			return obj.__dict__
		elif isinstance(obj, TestResult):
			return obj.__dict__
		elif isinstance(obj, TestSuite):
			return obj.__dict__
		elif isinstance(obj, Path):
			return str(obj)
		return json.JSONEncoder.default(self, obj)


# test supporting the source/spirv/native compilations modes
compilation_modes = [
	'api',
	'allocations',
	'atomics',
	'basic',
	'buffers',
	'c11_atomics',
	'compiler',
	'commonfns',
	'contractions',
	'conversions',
	'device_execution',
	'device_partition',
	'events',
	'generic_address_space',
	'geometrics',
	'gl_sharing',
	'half',
	'images_image_streams',
	'images_kernel_image_methods',
	'images_samplerless_reads',
	'integer_ops',
	'math_brute_force',
	'multiple_device_context',
	'non_uniform_work_group',
	'pipes',
	'printf',
	'profiling',
	'relationals',
	'select',
	'subgroups',
	'SVM',
	'thread_dimensions',
	'vectors',
	'workgroups',
]

# todo: headers
# todo: spir
# todo: more image test combinations
# todo: clcpp
# todo: gl
# todo: gles
testSuites = {k: TestSuite.create(k, v) for k, v in {
	'allocations': 'allocations/test_allocations',
	'api': 'api/test_api',
	'atomics': 'atomics/test_atomics',
	'basic': 'basic/test_basic',
	'buffers': 'buffers/test_buffers',
	'c11_atomics': 'c11_atomics/test_c11_atomics',
	'commonfns': 'commonfns/test_commonfns',
	'compiler': 'compiler/test_compiler',
	'computeinfo': 'computeinfo/test_computeinfo',
	'contractions': 'contractions/test_contractions',
	'conversions': 'conversions/test_conversions',
	'device_execution': 'device_execution/test_device_execution',
	'device_partition': 'device_partition/test_device_partition',
	'device_timer': 'device_timer/test_device_timer',
	'events': 'events/test_events',
	'generic_address_space': 'generic_address_space/test_generic_address_space',
	'geometrics': 'geometrics/test_geometrics',
	'half': 'half/test_half',
	'images_cl_copy_images': 'images/clCopyImage/test_cl_copy_images',
	'images_cl_fill_images': 'images/clFillImage/test_cl_fill_images',
	'images_cl_get_info': 'images/clGetInfo/test_cl_get_info',
	'images_cl_read_write_images': 'images/clReadWriteImage/test_cl_read_write_images',
	'images_image_streams': 'images/kernel_read_write/test_image_streams',
	'images_kernel_image_methods': 'images/kernel_image_methods/test_kernel_image_methods',
	'images_samplerless_reads': 'images/samplerlessReads/test_samplerless_reads',
	'integer_ops': 'integer_ops/test_integer_ops',
	'math_brute_force': 'math_brute_force/test_bruteforce',
	'mem_host_flags': 'mem_host_flags/test_mem_host_flags',
	'multiple_device_context': 'multiple_device_context/test_multiples',
	'non_uniform_work_group': 'non_uniform_work_group/test_non_uniform_work_group',
	'pipes': 'pipes/test_pipes',
	'printf': 'printf/test_printf',
	'profiling': 'profiling/test_profiling',
	'relationals': 'relationals/test_relationals',
	'select': 'select/test_select',
	'spirv': 'spirv_new/test_spirv_new',
	'subgroups': 'subgroups/test_subgroups',
	'SVM': 'SVM/test_svm',
	'thread_dimensions': 'thread_dimensions/test_thread_dimensions',
	'vectors': 'vectors/test_vectors',
	'workgroups': 'workgroups/test_workgroups',
}.items()}

disabled = {
	'cl_ext_cxx_for_opencl': 'extensions/cl_ext_cxx_for_opencl/test_cl_ext_cxx_for_opencl',
	'cl_khr_command_buffer': 'extensions/cl_khr_command_buffer/test_cl_khr_command_buffer',
	'cl_khr_semaphore': 'extensions/cl_khr_semaphore/test_cl_khr_semaphore',
	'gl': 'gl/test_gl',
	'gles': 'gles/test_gles',
	'spir': 'spir/test_spir',
	'vulkan': 'vulkan/test_vulkan',
}

desc_str = 'Pass %u Fails %u Crashes %u Timeouts %u'

def update_str():
	return desc_str % (len(result.passes), len(result.fails), len(result.crashes), len(result.timeouts))

def update_desc():
	pbar.set_description(update_str())

def update(res):
	if res.timedout:
		result.timeouts.append(res)
	elif res.returncode == 0:
		result.passes.append(res)
	elif res.returncode in [-6, -11]:
		result.crashes.append(res)
	else:
		result.fails.append(res)
	update_desc()
	pbar.update()

def poolError(*err):
	print('pool error')
	print(err)

includes = set(args.include)
excludes = set(args.exclude)

def filter(t):
	res = [(t, test) for test in t.tests]
	if len(includes) > 0:
		return res if t.id in includes else []
	else:
		return res if t.id not in excludes else []

tests = []
if (len(args.test_file) > 0):
	for f in args.test_file:
		path = Path(f)
		content = path.read_text('utf-8')
		for line in content.splitlines():
			tuple = line.split(' ')
			tests.append((testSuites[tuple[0]], tuple[1]))
else:
	tests = list(itertools.chain.from_iterable(map(filter, testSuites.values())))

pool = multiprocessing.Pool(1 if args.singlethreaded else args.jobs)
pbar = tqdm(total=len(tests))
update_desc()

for t in tests:
	pool.apply_async(TestSuite.execute, t, callback=update, error_callback=poolError)
pool.close()

try:
	pool.join()
except (KeyboardInterrupt, SystemExit):
	None
finally:
	TestSuite.killAll()
pbar.close()

#print(result)
print('\npasses:')
for tr in sorted(result.passes):
	print('%s %s' % (tr.id, tr.name))

print('\nfails:')
for tr in sorted(result.fails):
	print('%s %s' % (tr.id, tr.name))

print('\ncrashes:')
for tr in sorted(result.crashes):
	print('%s %s' % (tr.id, tr.name))

print('\ntimeouts:')
for tr in sorted(result.timeouts):
	print('%s %s' % (tr.id, tr.name))

print('\n' + update_str())
